import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class db{

  static Database _database;

  Future<Database> get database async {
    if (_database == null) {
      return await inicial();
    }
    return _database;
  }

  inicial() async{
    return await openDatabase(
      join(await getDatabasesPath(), 'persona.db'),
      onCreate: (Database db,int version) {
        return db.execute(
          "CREATE TABLE personas(id INTEGER PRIMARY KEY, nombre TEXT, direccion TEXT, telefono INTEGER)",
        );
      },
      version: 1,
    );
  }

  Future<void> insert(persona npersona) async {
    final Database db = await database;
      await db.insert(
        'personas',
        npersona.toMap(),
        conflictAlgorithm: ConflictAlgorithm.abort,
      );
  }

  Future<List<persona>> gente() async {
    final Database db = await database;

    final List<Map<String, dynamic>> maps = await db.query('personas');

    return List.generate(maps.length, (i) {
      return persona(
          id: maps[i]['id'],
          nombre: maps[i]['nombre'],
          direccion: maps[i]['direccion'],
          telefono: maps[i]['telefono']
      );
    });
  }

  Future<persona> obtener(int id) async {
    final Database db = await database;

    final List<Map<String, dynamic>> maps = await db.query('personas',
      where: "id = ?",
      whereArgs: [id],
    );

    if (maps.isEmpty){
      return null;
    }
    else {
      return persona(
          id: maps[0]['id'],
          nombre: maps[0]['nombre'],
          direccion: maps[0]['direccion'],
          telefono: maps[0]['telefono']
      );
    }
  }

  Future<void> update(persona upersona) async {
    final db = await database;

    await db.update(
      'personas',
      upersona.toMap(),
      where: "id = ?",
      whereArgs: [upersona.id],
    );
  }

  Future<void> deletepersona(int id) async {
    final db = await database;

    await db.delete(
      'personas',
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<bool> buscar(int id) async {
    final db = await database;

    final List<Map<String, dynamic>> maps =await db.query(
      'personas',
      where: "id = ?",
      whereArgs: [id],
    );
    if(maps.isEmpty){
      return false;
    }
    else{
      return true;
    }
  }
}

class persona {
  int id;
  String nombre;
  String direccion;
  int telefono;

  persona({this.id,this.nombre, this.direccion, this.telefono});

  Map<String, dynamic> toMap() {
    return {
      'id':id,
      'nombre': nombre,
      'direccion': direccion,
      'telefono': telefono,
    };
  }
}
