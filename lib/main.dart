import 'package:flutter/material.dart';
import './db.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'topicos 2',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'persistencia de datos'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var CI = TextEditingController();
  var Nombre = TextEditingController();
  var Direccion = TextEditingController();
  var Telefono = TextEditingController();
  List<persona> personas=[];
  bool existe=false;

  Future<void> vacios() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Campos en blanco, revise e intente nuevamente.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context){
    db().gente().then((resultat){
      setState(() => personas=resultat);
    });
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('CI: '),
                  Container(
                    width: 200,
                    margin: new EdgeInsets.symmetric(vertical: 10),
                    child:TextFormField(
                      textAlign: TextAlign.center,
                      controller: CI,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.all(8),
                        isDense: true,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('nombre: '),
                  Container(
                    width: 300,
                    margin: new EdgeInsets.symmetric(vertical: 10),
                    child:TextFormField(
                      textAlign: TextAlign.center,
                      controller: Nombre,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.all(8),
                        isDense: true,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('direccion: '),
                  Container(
                    width: 300,
                    margin: new EdgeInsets.symmetric(vertical: 10),
                    child:TextFormField(
                      textAlign: TextAlign.center,
                      controller: Direccion,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.all(8),
                        isDense: true,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('telefono: '),
                  Container(
                    width: 200,
                    margin: new EdgeInsets.symmetric(vertical: 10),
                    child:TextFormField(
                      textAlign: TextAlign.center,
                      controller: Telefono,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.all(8),
                        isDense: true,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child:RaisedButton(
                        color: Colors.indigo,
                        textColor: Colors.white,
                        child: Text(
                          "Guardar",
                          style: TextStyle(
                            fontSize: 20.0,
                          ),
                        ),
                        onPressed: () {
                          if(CI.text!="" && Nombre.text!="" && Direccion.text!="" && Telefono.text!="") {
                            db().buscar(int.parse(CI.text)).then((resultat) {
                              setState(() => existe = resultat);
                            });
                            persona nuevo = persona(
                                id: int.parse(CI.text),
                                nombre: Nombre.text,
                                direccion: Direccion.text,
                                telefono: int.parse(Telefono.text)
                            );
                            if (!existe) {
                              db().insert(nuevo).then((resultat) {
                                setState(() {});
                              });
                              ;
                            }
                            else {
                              db().update(nuevo).then((resultat) {
                                setState(() {});
                              });
                            }
                          }
                          else{
                            vacios();
                          }
                        }),
                  ),
                ],
              ),
            ),
            Container(
              child: Table(
                border: TableBorder.all(width:1.0, color: Colors.black),
                children: [
                  TableRow(
                    children: [
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new Text('CI'),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new Text('Nombre'),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new Text('Direccion'),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new Text('Telefono'),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new Text(''),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                          new Text(''),
                        ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              child: Table(
                border: TableBorder.all(width:1.0, color: Colors.black),
                children: personas.map((category) =>
                  TableRow(
                    children: [
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new Text(category.id.toString()),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new Text(category.nombre),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new Text(category.direccion),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new Text(category.telefono.toString()),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new RaisedButton(
                              color: Colors.indigo,
                              textColor: Colors.white,
                              child: Text(
                                "Modificar",
                              style: TextStyle(
                                fontSize: 7.0,
                              ),
                              ),
                            onPressed: () {
                              db().obtener(category.id).then((resultat){
                                setState(() => {
                                  CI.text=resultat.id.toString(),
                                  Nombre.text=resultat.nombre,
                                  Direccion.text=resultat.direccion,
                                  Telefono.text=resultat.telefono.toString(),
                                });
                              });
                              setState(() {});
                            }),
                          ],
                        ),
                      ),
                      TableCell(
                        child: Column(
                          children: <Widget>[
                            new RaisedButton(
                                color: Colors.indigo,
                                textColor: Colors.white,
                                child: Text(
                                  "Eliminar",
                                  style: TextStyle(
                                    fontSize: 7.0,
                                  ),
                                ),
                                onPressed: () {
                                  db().deletepersona(category.id);
                                  setState(() {});
                                }),
                          ],
                        ),
                      ),
                    ]
                  ),
                ).toList(),
              ),
            ),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
